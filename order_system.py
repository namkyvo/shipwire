"""
Program name: order_system.py
Version: 1
Author:  Nam Vo   (nam.vo@virtualinstruments.com)
Purpose: A simple order processing system using thread and synchronized queue.
Description:
- Data source:
  There should be a data source capable of generating one or more streams of orders.
  An order consists of a unique identifier (per stream) we will call the "header", and a demand for between zero and five units each of A,B,C,D, and E, except that there must be at least one unit demanded.
  A valid order (in whatever format you choose): {"Header": 1, "Lines": {"Product": "A", "Quantity": "1"},{"Product": "C", "Quantity": "4"}}
  An invalid order: {"Header": 1, "Lines": {"Product": "B", "Quantity": "0"}}

  


"""

#Import

import threading
import time
import logging
import random
import sys
from collections import defaultdict
import queue

#Logging setup
logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s',)

#Common
order_processing_queue = queue.Queue()


# == start class definitions
"""
Class name: Inventory
Description: Holding system's inventory and keep processed orders and backorders for each of product.
             Inventory is updated for every processed order.
             
           - Inventory allocator: allocates inventory to the inbound data according to the following rules:
             1) Inbound orders to the allocator should be individually identifyable (ie two streams may generate orders
                with an identical header, but these orders should be identifyable from their streams)
             2) Inventory should be allocated on a first come, first served basis; once allocated, inventory is not available
                to any other order.
             3) Inventory should never drop below 0.
             4) If a line cannot be satisfied, it should not be allocated.  Rather, it should be  backordered
                (but other lines on the same order may still be satisfied).
             5) When all inventory is zero, the system should halt and produce output listing,
                in the order received by the system,
                the header of each order, the quantity on each line, the quantity allocated to each line,
                and the quantity backordered for each line.
"""

class Inventory:
    class product_history:
        def __init__(self,quantity):
            """ order history for a product:
                order ID, quantity allocated
            """
            self._order_history = {}
            self._quantity = quantity
            """
            Keep order ID and quantity
            """
            self._backlog_orders = {}

        def _get_product_quantity(self):
            return self._quantity
        def _set_product_quantity(self, new_quantity):
            self._quantity = new_quantity
            
        def _get_product_orders_history(self):
            return self._order_history
        def _get_product_backlog_orders(self):
            return self._backlog_orders
        def _set_product_orders_history(self,order_id, quantity_allocated):
            self._order_history[order_id] = quantity_allocated

        def _set_product_backlog_orders(self, order_id, quantity_backorder):
            self._backlog_orders[order_id] = quantity_backorder

     
            
    def __init__(self):
        """
        inventory_index: a list of products sorted in alphabetical order for fast indexing
        """
        self.inventory_index = []
        """
        inventory: a map of product names to their processed order info
        """
        self.inventory = defaultdict(Inventory.product_history)

        with open('inventory.dat','r') as f:
            inventory_data = f.readlines()
        for line in inventory_data:
            (product_name, quantity) = (line.strip()).split(':')
            self.inventory[product_name] = Inventory.product_history(int(quantity))
        
        self.inventory_index = sorted(self.inventory, key=lambda x: x[0])

    def _show_inventory(self):
        for name, quantity in self.inventory.items():
            print("Product: %s  Quantity: %s" % (name, quantity.get_product_quantity()))
    def _show_inventory_index(self):
        print (self.inventory_index)
        
    def _show_inventory_backlog(self):
        print("Backlog for all product:")
        for name, item in self.inventory.items():
            print("Product: %s " % (name), end='')
            backlog = item.get_product_backlog_orders()
            for order_id, quantity in backlog.items():
                print("Order ID: %s, backlog: %s " % (order_id, quantity), end='')
            print('')
    
    def _show_inventory_history(self):
        print("Order history for all product:")
        for name, item in self.inventory.items():
            print("Product: %s " % (name), end='')
            orders = item.get_product_orders_history()
            for order_id, quantity in orders.items():
                print("Order ID: %s, quantity: %s " % (order_id, quantity),end='')
            print('')
    
    def _get_total_inventory(self):
        total = 0
        for product, inventory in self.inventory.items():
            total += inventory._get_product_quantity()

        return total
    def _get_inventory_index(self):
        return self.inventory_index
    def _get_inventory(self):
        return self.inventory

    def _inventory_allocator(self,order):
        order_id = order['Header']
        order_data = order['Lines']
        #print("Receiving order: ID: %s, order details: %s" % (order_id, order_data))
        for each_item in order_data:
            product = each_item['Product']
            order_quantity = int(each_item['Quantity'])
            if product in self.inventory:
                current_inventory_quantity = self.inventory[product]._get_product_quantity()
                #print("Current inventory quantity for product: %s   %d" % (product,current_inventory_quantity))
            else:
                print("ERROR: Inventory does not have product: %s" % (product))

            if current_inventory_quantity >= order_quantity:
                new_quantity = current_inventory_quantity - order_quantity
                self.inventory[product]._set_product_quantity(new_quantity)
                self.inventory[product]._set_product_orders_history(order_id,order_quantity)
                
            else:
                new_quantity = 0
                quantity_allocated = current_inventory_quantity
                quantity_backlog = order_quantity - current_inventory_quantity
                self.inventory[product]._set_product_quantity(new_quantity)
                self.inventory[product]._set_product_orders_history(order_id,quantity_allocated)
                self.inventory[product]._set_product_backlog_orders(order_id, quantity_backlog)
                
"""
Class name: Orders
Description: simulates a stream of orders by reading in an file containing orders. Orders are processed
             in first come first serve basic and are sent to Inventory for processing.
             A synchronized queue is used to send and receive orders between Orders and System.
             Orders received by System are sent to Inventory for processing.
      
           - Order format:
             An order consists of a unique identifier (per stream) we will call the "header",
             and a demand for between zero and five units each of A,B,C,D, and E, except that there must be at
             least one unit demanded.
             A valid order (in whatever format you choose):
             {"Header": 1, "Lines": {"Product": "A", "Quantity": "1"},{"Product": "C", "Quantity": "4"}}
             An invalid order: {"Header": 1, "Lines": {"Product": "B", "Quantity": "0"}}
"""  
class Orders(threading.Thread):

    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None, verbose=None):
        super(Orders,self).__init__()
        self._stopevent = threading.Event()
        self._sleepperiod = 1.0
        self.target = target
        self.name = name
        #self.count=0
        self._orders_queue = []
        self._order_history = {}
        
        with open('orders.dat','r') as fhd:
            self.orders = fhd.readlines()
            for order in self.orders:
                order = eval(order)
                if self._isvalid_order(order):
                    self._orders_queue.append(order)
                    order_id = order['Header']
                    order_data = order['Lines']
                    self._order_history[order_id] = order_data
                else:
                    continue
        
    def _isvalid_order(self,order):
        """
        Basic error checking for orders:
          - Return False if order data is empty
          - Return False if any order quantity is 0
        """
        order_id = order['Header']
        order_data = order['Lines']
        if not order_data:
            return False
        for each_item in order_data:
            quantity = each_item['Quantity']
            product = each_item['Product']
            if int(quantity) == 0:
                #print("ERROR: Order ID: %s, product: %s, quantity: %s" % (order_id,product,quantity))
                return False
        return True
    
    def _get_orders_history(self):
        return self._order_history
        
    def _show_orders(self):
        for order in self._orders_queue:
            print("Order Id: %s, order data: %s" % (order['Header'], order['Lines']))

    def run(self):
        while not self._stopevent.isSet():
            lenq = len(self._orders_queue)
            if lenq != 0:
                order = self._orders_queue.pop(0)
                if not order_processing_queue.full():
                    order_processing_queue.put(order)
                    logging.debug('Putting ' + str(order)  
                              + ' : ' + str(order_processing_queue.qsize()) + ' items in queue')
            self._stopevent.wait(self._sleepperiod)
            time.sleep(random.random())
  
        return
    
    def join(self,timeout=None):
        self._stopevent.set()
        threading.Thread.join(self, timeout)

"""
Class name: System
Description: simulates an order processing system. When system starts, it initilizes inventory and orders.
             A synchronized queue is utilized to faciliate sending and receiving orders from Orders to System.
             Orders are processed in system in first come first serve basic and sent to Inventory for processing
             and inventory level is tracked.
             When all inventory is zero, the system should halt and produce output listing, in the order
             received by the system,the header of each order, the quantity on each line, the quantity allocated
             to each line, and the quantity backordered for each line.
"""
class System(threading.Thread):
    """
    Class name: System
    """
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None, verbose=None):
        super(System,self).__init__()
        self._stopevent = threading.Event()
        self.target = target
        self._sleepperiod = 1.0
        self.name = name
        self.inventory = Inventory()
        self.orders = Orders()
        return

    def run(self):
        while not self._stopevent.isSet():
            if not order_processing_queue.empty():
                order = order_processing_queue.get()
                logging.debug('Getting ' + str(order) 
                              + ' : ' + str(order_processing_queue.qsize()) + ' items in queue')
                self.process_order(order)

                      
            self._stopevent.wait(self._sleepperiod)
            time.sleep(random.random())

    def join(self, timeout=None):
        self._stopevent.set()
        logging.debug('joining')
        threading.Thread.join(self,timeout)
    
    def process_order(self,order):
        self.inventory._inventory_allocator(order)
            
    def total_inventory_quantity(self):
        return self.inventory._get_total_inventory()

    def print_system_status(self):
        """
        Print order history for all order id with format:
       order_id:quantity,,,::quantity allocated,,,::quantity backlog
        1: 1,0,1,0,0::1,0,1,0,0::0,0,0,0,0
        2: 0,0,0,0,5::0,0,0,0,0::0,0,0,0,5
        3: 0,0,0,4,0::0,0,0,0,0::0,0,0,4,0
        4: 1,0,1,0,0::1,0,0,0,0::0,0,1,0,0
        5: 0,3,0,0,0::0,3,0,0,0::0,0,0,0,0
        """
        order_history = self.orders._get_orders_history()
        inventory_index = self.inventory._get_inventory_index()
        for order_id, order_info in order_history.items():
            #=====================================================
            #print original orders
            #=====================================================
            print("id:%s " % order_id, end='')
            order_list = [0]*len(inventory_index)
            for order in order_info:
                product = order['Product']
                quantity = order['Quantity']
                #Find index of product
                index = inventory_index.index(product)
                order_list[index] = int(quantity)
                
            print(order_list,"::",end='')
            #=====================================================
            #print processed orders
            #=====================================================
            order_allocated_list = [0]*len(inventory_index)
            inventory_data = self.inventory._get_inventory()

            for order in order_info:
                product = order['Product']
                index = inventory_index.index(product)
                order_history = inventory_data[product]
                order_allocated_history = order_history._get_product_orders_history()
                #Search for order id in history
                #print(order_allocated_history)
                if order_id in order_allocated_history:
                    allocated_quantity = order_allocated_history[order_id]
                    order_allocated_list[index] = allocated_quantity
             
            print(order_allocated_list,"::",end='')
            #=====================================================
            # print backlog order
            #=====================================================
            backlog_orders_list = [0]*len(inventory_index)
            for order in order_info:
                product = order['Product']
                index = inventory_index.index(product)
                order_history = inventory_data[product]
                backlog_orders = order_history._get_product_backlog_orders()
                #Search for order id in backlog orders
                if order_id in backlog_orders:
                    backlog_quantity = backlog_orders[order_id]
                    backlog_orders_list[index] = backlog_quantity
            print(backlog_orders_list)
            
                
    def _get_orders_history(self):
        return self.order_history
        
    def _show_orders(self):
        for order in self.orders_queue:
            print("Id: %s, order data: %s" % (order['Header'], order['Lines']))
            
if __name__ == '__main__':
    
    orders = Orders(name='orders')
    system = System(name='system')

    orders.start()
    time.sleep(2)
    system.start()
    while(True):
        total = system.total_inventory_quantity()
        if total == 0:
            system.print_system_status()
            orders.join()
            system.join()
            sys.exit()
        else:
            time.sleep(2)
  
     
