# README #

shipwire candidate project
==========================
1. Number of Files: 4
2. List of Files:
   - order_system.py     :   python script to implement the order processing system.
   - orders.dat          :   orders data file.
   - inventory.dat       :   inventory data file.

3. Installation and execution:
   Intepreter requiements: 
   - python    : version 3.3 or above.
   - python packages:   collections, logging, queue,


   Copy inventory.dat, order_system.py, and orders.dat to a directory. 
   Install python 3.3 if needed. You can execute the project in IDLE or
   from command prompt with command line: python order_system.py
